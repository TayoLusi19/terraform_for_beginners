# AWS Terraform Configuration for Web Server Setup

## Introduction
This Terraform project configures an AWS infrastructure to host a web server. It creates a VPC, Internet Gateway, Route Tables, Subnets, Security Groups, and an EC2 instance with Apache2 installed.

## Prerequisites
- AWS account.
- Terraform installed on your machine.
- AWS CLI configured with your credentials.

## Configure the AWS Provider
Avoid hardcoding AWS credentials directly in Terraform files. For testing purposes, you may input them, but for production, use environment variables or IAM roles.

```hcl
provider "aws" {
  region     = "us-east-1"
  access_key = "input your access key here"
  secret_key = "input your secret key here"
}
```

## 1. Create VPC

Creates a VPC named tayo_vpc with a CIDR block of 10.0.0.0/16, establishing the network environment.

```hcl
resource "aws_vpc" "tayo_vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "production"
  }
}
```

## 2. Internet Gateway
Sets up an Internet Gateway gw for the VPC to allow internet access.

## 3. Custom Route Table
Creates a Route Table r in tayo_vpc to define rules for traffic routing, including IPv4 and IPv6 routes.

## 4. Subnet
Defines a subnet subnet_1 within tayo_vpc. The CIDR block can be set via the subnet_prefix variable.

## 5. Subnet-Route Table Association
Associates subnet_1 with the r route table to facilitate network traffic flow.

## 6. Security Group
Establishes a Security Group allow_web in the VPC, allowing specific inbound traffic (SSH, HTTP, HTTPS) and unrestricted outbound traffic.

##v7. Network Interface
Creates a Network Interface web_server_nic in subnet_1 with a static IP and associates it with allow_web security group.

## 8. Elastic IP
Allocates an Elastic IP one and associates it with web_server_nic, ensuring a static public IP.

## 9. Ubuntu Server with Apache2
Deploys an EC2 instance ec2 using Ubuntu AMI. It attaches web_server_nic and runs a script to install Apache2. The server is tagged as "EC2_Instance".

## Outputs
1. server_public_ip: The public IP of the EC2 instance.
2. server_private_ip: The private IP of the EC2 instance.
3. server_id: The ID of the EC2 instance.

## Deployment Steps
1. Initialize Terraform: Run terraform init.
2. Review Plan: Execute terraform plan.
3. Apply Configuration: Use terraform apply.

## Cleanup
Run terraform destroy to remove all resources and avoid unwanted AWS charges.

## Security Note
Remember to handle AWS credentials securely and avoid publishing them in your Terraform files or public repositories.

